﻿using Bronze.Common.Data.Game.CellFeatures;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Dungeoneering.Logic
{
    public class DungeonCellFeature : AbstractCellFeature
    {
        public DungeonDefinition DungeonDefinition { get; set; }
        public ICellFeature PostTransformFeature { get; set; }
        public  ICellDevelopmentType PostTransformDevelopment { get; set; }

        public DungeonCellFeature()
        {
        }
        
        public override void ApplyGeneration(IWorldManager worldManager, District district, Tile[,] tiles)
        {
        }
    }
}
