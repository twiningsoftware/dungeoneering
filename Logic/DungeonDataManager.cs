﻿using System.Collections.Generic;

namespace Dungeoneering.Logic
{
    /// <summary>
    /// Default implementation of IDungeonDataManager.
    /// </summary>
    public class DungeonDataManager : IDungeonDataManager
    {
        public List<DungeonDefinition> DungeonDefinitions { get; }

        public DungeonDataManager()
        {
            DungeonDefinitions = new List<DungeonDefinition>();
        }
    }
}
