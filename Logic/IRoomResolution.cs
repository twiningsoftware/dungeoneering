﻿using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Xml.Linq;

namespace Dungeoneering.Logic
{
    /// <summary>
    /// An option for resolving a dungeon room encounter, this is presented to the player as buttons on the UI.
    /// </summary>
    public interface IRoomResolution
    {
        string Text { get; }

        IfState[] IfStates { get; }

        IRoomResolution LoadFrom(IInjectionProvider injectionProvider, IXmlReaderUtil xmlreaderUtil, XElement element, DungeonDefinition dungeonDefinition);

        ResolutionData GetResolution(Random random, NotablePerson character, IUnit bodyguard);
    }
}