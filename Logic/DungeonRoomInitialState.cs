﻿namespace Dungeoneering.Logic
{
    public class DungeonRoomInitialState
    {
        public string State { get; set; }
        public string Value { get; set; }
    }
}
