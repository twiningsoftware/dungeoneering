﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.UI;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Dungeoneering.Logic.ResolutionActions
{
    /// <summary>
    /// This is a resolution action that changes the current room's state.
    /// </summary>
    public class SetCurrentRoomStateResolutionAction : IResolutionAction
    {
        private string _state;
        private string _value;
        
        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            _state = xmlreaderUtil.AttributeValue(element, "state");
            _value = xmlreaderUtil.AttributeValue(element, "value");
            
            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {
            if(dungeon.CurrentRoom.RoomState.ContainsKey(_state))
            {
                dungeon.CurrentRoom.RoomState[_state] = _value;
            }
            else
            {
                dungeon.CurrentRoom.RoomState.Add(_state, _value);
            }
            return Enumerable.Empty<IUiElement>();
        }
    }
}
