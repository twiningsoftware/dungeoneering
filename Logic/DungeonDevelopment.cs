﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;

namespace Dungeoneering.Logic
{
    public class DungeonDevelopment : AbstractCellDevelopment<DungeonDevelopmentType>, IArmyInteractable
    {
        public string InteractVerb => "Exploring";
        public bool InteractAdjacent => true;
        public bool InteractWithForeign => true;
        public override bool CanBeSieged => false;
        public override bool CanBeDestroyed => false;

        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private readonly IDungeonDataManager _dungeonDataManager;

        private GeneratedDungeon _generatedDungeon;
        
        public DungeonDevelopment(
            IDialogManager dialogManager, 
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData,
            IDungeonDataManager dungeonDataManager)
            : base(dialogManager, gamedataTracker)
        {
            _playerData = playerData;
            _dialogManager = dialogManager;
            _dungeonDataManager = dungeonDataManager;
        }

        public void BuildDungeon(Random random, DungeonDefinition dungeonDefinition)
        {
            _generatedDungeon = new GeneratedDungeon(random.Next(), dungeonDefinition);
        }
        
        public void DoInteraction(Army interactingArmy)
        {
            if (interactingArmy.Owner == _playerData.PlayerTribe
                && _generatedDungeon != null)
            {
                _dialogManager.PushModal<RunDungeonDialog, RunDungeonDialogContext>(
                    new RunDungeonDialogContext
                    {
                        GeneratedDungeon = _generatedDungeon,
                        Character = interactingArmy.General,
                        Army = interactingArmy,
                    });
            }
        }

        public override void SerializeTo(SerializedObject developmentRoot)
        {
            base.SerializeTo(developmentRoot);

            _generatedDungeon.SerializeTo(developmentRoot.CreateChild("generated_dungeon"));
        }

        public override void DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            SerializedObject developmentRoot)
        {
            base.DeserializeFrom(gamedataTracker, developmentRoot);

            _generatedDungeon = GeneratedDungeon.DeserializeFrom(
                _dungeonDataManager,
                developmentRoot.GetChild("generated_dungeon"));
        }
    }
}
