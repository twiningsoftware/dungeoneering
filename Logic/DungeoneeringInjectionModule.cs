﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Dungeoneering.Logic.ResolutionActions;

namespace Dungeoneering.Logic
{
    /// <summary>
    /// This hooks into Bronze Age's startup process to register new services.
    /// </summary>
    public class DungeoneeringInjectionModule : IInjectionModule
    {
        public void RegisterInjections(IInjector injector)
        {
            // Data Loaders
            injector.RegisterService<DungeonDataLoader>();
            injector.RegisterService<DungeonCellFeaturePostLinker>();

            // Services
            injector.RegisterService<DungeonDataManager>();
            injector.RegisterService<RunDungeonCommand>();
            injector.RegisterService<WorldDungeonGenerator>();

            // Dialogs
            injector.RegisterDialog<RunDungeonDialog>();

            // Room Resolutions
            injector.RegisterNamedType<StatCheckResolution, IRoomResolution>();
            injector.RegisterNamedType<AutoPassResolution, IRoomResolution>();

            // Resolution Actions
            injector.RegisterNamedType<RoomTransformResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<TransitionRoomResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<BodyguardHealthResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<GrantSkillResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<TransitionExitResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<SetCurrentRoomStateResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<AddSkillResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<ChanceModifiedResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<UnitRewardResolutionAction, IResolutionAction>();

            // Cell Developments
            injector.RegisterDevelopmentType<DungeonDevelopmentType, DungeonDevelopment>();

            // Cell Features
            injector.RegisterNamedType<DungeonCellFeature, ICellFeature>();


            //TODO how to get dungeon cell developments
            // idea, have a cell feature that can be placed as normal during world generation
            // then, after world generation a service finds those and turns them into dungeon cell developments, generating and saving the dungeon
            // those can then be interactables, using that functionality to run the dungeon as normal
            // also define a point of interest tracker that creates events, make it extensible so it can be used for discovering resources as well
        }
    }
}
