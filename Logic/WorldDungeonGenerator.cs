﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;

namespace Dungeoneering.Logic
{
    public class WorldDungeonGenerator : IWorldStateService
    {
        public void Clear()
        {
        }

        public void Initialize(WorldState worldState)
        {
            var random = new Random(worldState.Seed.GetHashCode());

            foreach(var plane in worldState.Planes)
            {
                foreach(var region in plane.Regions)
                {
                    foreach(var cell in region.Cells)
                    {
                        if(cell.Feature != null && cell.Feature is DungeonCellFeature dungeonCellFeature)
                        {
                            dungeonCellFeature.PostTransformDevelopment
                                .Place(null, cell, true);

                            cell.Feature = dungeonCellFeature.PostTransformFeature;

                            if(cell.Development is DungeonDevelopment dungeonDevelopment)
                            {
                                dungeonDevelopment.BuildDungeon(random, dungeonCellFeature.DungeonDefinition);
                            }
                        }
                    }
                }
            }
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }
    }
}
