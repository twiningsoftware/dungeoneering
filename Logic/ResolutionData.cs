﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.InjectableServices;
using Dungeoneering.Logic.ResolutionActions;

namespace Dungeoneering.Logic
{
    /// <summary>
    /// A container for data on how to apply a room resolution.
    /// </summary>
    public class ResolutionData
    {
        public string Description { get; private set; }

        public IResolutionAction[] ResolutionActions { get; private set; }

        public static ResolutionData LoadFrom(
            IInjectionProvider injectionProvider,
            IXmlReaderUtil xmlReaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            var descriptionElement = xmlReaderUtil.Element(element, "description");

            var data = new ResolutionData
            {
                Description = descriptionElement.Value,
                ResolutionActions =
                    element.Elements("action")
                        .Select(e => injectionProvider
                            .BuildNamed<IResolutionAction>(xmlReaderUtil.AttributeValue(e, "type"))
                            .LoadFrom(xmlReaderUtil, e, dungeonDefinition))
                    .ToArray()
            };
            
            // TODO transition_room
            // TODO transition_exit
            // TODO <reward type="BodyguardHealthPerc" value="-0.2" />
            // TODO <reward type="Skill" skill_id="loot_golden_neckband" level="0" />

            return data;
        }
    }
}
