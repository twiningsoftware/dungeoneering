﻿using Bronze.Common.DataLoaders;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Dungeoneering.Logic
{
    public class DungeonCellFeaturePostLinker : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IDungeonDataManager _dungeonDataManager;

        public override string ElementName => "cell_feature";

        public DungeonCellFeaturePostLinker(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            IDungeonDataManager dungeonDataManager)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _dungeonDataManager = dungeonDataManager;

        }

        public override void Load(XElement element)
        {
        }

        public override void PostLoad(XElement element)
        {
            var cellFeature = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "id",
                _gamedataTracker.CellFeatures,
                cf => cf.Id);

            if (cellFeature is DungeonCellFeature dungeonCellFeature)
            {
                dungeonCellFeature.DungeonDefinition = _xmlReaderUtil
                    .ObjectReferenceLookup(
                        element,
                        "dungeon_id",
                        _dungeonDataManager.DungeonDefinitions,
                        dd => dd.Id);


                dungeonCellFeature.PostTransformDevelopment = _xmlReaderUtil
                    .ObjectReferenceLookup(
                        element,
                        "post_transform_cell_development_id",
                        _gamedataTracker.CellDevelopments.OfType<DungeonDevelopmentType>(),
                        cd => cd.Id);

                //dungeonCellFeature.PostTransformFeature = _xmlReaderUtil
                //    .OptionalObjectReferenceLookup(
                //        element,
                //        "post_transform_cell_feature_id",
                //        _gamedataTracker.CellFeatures,
                //        cf => cf.Id);
            }
        }
    }
}
