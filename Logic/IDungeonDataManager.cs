﻿using System.Collections.Generic;

namespace Dungeoneering.Logic
{
    /// <summary>
    /// Defines a service that stores DungeonDefinitions.
    /// </summary>
    public interface IDungeonDataManager
    {
        List<DungeonDefinition> DungeonDefinitions { get; }
    }
}
